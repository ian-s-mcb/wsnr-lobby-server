# wsnr-lobby-server

A game lobby web app built with WebSockets, Node.js, and React.
Currently supports Tic-Tac-Toe gameplay.

This repo contains the server codebase. See [this repo][client] for the
client code.

### [Demo][demo]

### Screencast

![Video screencast of game lobby app in action][screencast]

### Running locally

```bash
# In first terminal
git clone https://gitlab.com/ian-s-mcb/wsnr-chat-server
cd ./wsnr-chat-server
yarn
yarn dev

# In second terminal
git clone https://gitlab.com/ian-s-mcb/wsnr-chat-client
cd ./wsnr-chat-client
yarn
yarn start

# Open web browser, navigate to https://localhost:3000
```

[screencast]: https://i.imgur.com/yUGVjyJ.gif
[demo]: https://wsnr-lobby-client.herokuapp.com/
[client]: https://gitlab.com/ian-s-mcb/wsnr-chat-server
