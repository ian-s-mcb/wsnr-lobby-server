import {
  buildMessage, nicknameAndReadyStatus, sendMessage
} from './utils'

// Close socket connection callback
function handleClose (clients, client) {
  // Disregard if client not found
  const i = clients.findIndex(c => c.id === client.id)
  if (i === -1) { return }

  // Build message
  const remainingClients = clients
    .filter(c => c.id !== client.id)
  const members = nicknameAndReadyStatus(remainingClients)
  const messageTitle = `${client.nickname} has disconnected`
  const message = buildMessage(client, members, messageTitle)
  // Update clients list
  clients.splice(i, 1)

  // Send
  sendMessage(remainingClients, message)
}

export { handleClose }
