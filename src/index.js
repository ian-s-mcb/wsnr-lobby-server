import express from 'express'
import WebSocket from 'ws'
import uuid from 'node-uuid'

import * as constants from './constants'
import { handleClose } from './handleClose'
import { handleMessage } from './handleMessage'
import { buildAndSendMessage } from './utils'

const PORT = process.env.PORT || 5000
const app = express()

// Start HTTP server
const server = app.listen(PORT, function () {
  console.log(`Listening on port ${PORT}`)
})

// Create ws server
const wss = new WebSocket.Server({ server })

// Globals for ws
const clients = []

// ws logic
wss.on(constants.EVENT_CONN, ws => {
  // Create new client
  const client = {
    id: uuid.v4(),
    gameState: Array(9).fill(null),
    lonePlayer: true,
    nickname: '',
    opponent: null,
    player1Turn: true,
    players: { player1: null, player2: null },
    readyStatus: constants.STATUS_READY,
    rematchRequested: false,
    rematchRequester: null,
    tie: false,
    winner: null,
    winningLine: null,
    ws
  }
  clients.push(client)
  const messageTitle = 'A client has connected'
  buildAndSendMessage(clients, client, clients, messageTitle)

  // Handle message
  ws.on(constants.EVENT_MSG,
    message => handleMessage(clients, client, message)
  )

  // Handle connection close
  ws.on(constants.EVENT_CLOSE, () => handleClose(clients, client))
})
