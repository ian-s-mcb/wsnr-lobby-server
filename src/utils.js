// Send recipients a newly built message
function buildAndSendMessage (clients, client, recipients, messageTitle) {
  const members = nicknameAndReadyStatus(nicknamedClients(clients))
  const message = buildMessage(client, members, messageTitle)
  sendMessage(recipients, message)
}

// Build a message from client properties
function buildMessage (client, members, messageTitle) {
  const {
    gameState, lonePlayer, nickname, opponent, player1Turn, players,
    readyStatus, rematchRequested, rematchRequester, tie, winner,
    winningLine
  } = client

  const message = {
    gameState,
    lonePlayer,
    members,
    messageTitle,
    nickname,
    opponent,
    player1Turn,
    players,
    readyStatus,
    rematchRequested,
    rematchRequester,
    tie,
    winner,
    winningLine
  }

  return message
}

// Get nicknames, readyStatus for each client
const nicknameAndReadyStatus = clients => (
  clients.map(c => ({
    nickname: c.nickname,
    readyStatus: c.readyStatus
  }))
)

// Get clients with nicknames
const nicknamedClients = clients => (
  clients.filter(c => c.nickname)
)

// Send recipients the given message
function sendMessage (recipients, message) {
  recipients
    .forEach(r => {
      message.recipient = r.nickname
      r.ws.send(JSON.stringify(message))
    })
}

export {
  buildAndSendMessage,
  buildMessage,
  nicknameAndReadyStatus,
  nicknamedClients,
  sendMessage
}
