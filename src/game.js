import * as constants from './constants'
import { buildAndSendMessage } from './utils'

// Apply user's turn to game state
function applyTurn (clients, client, messageArray) {
  // Disregard if gameState argument is absent
  if (messageArray.length < 2) { return }

  let gameState = messageArray[1]
  const opponentName = client.opponent
  const { nickname, player1Turn, players } = client

  // Disregard if client doesn't have an opponent
  if (!opponentName || !players.player1 || !players.player2) { return }

  // Disregard if client isn't busy or cannot find opponent client
  const amIBusy = client.readyStatus === constants.STATUS_BUSY
  const opponent = clients.find(
    c => (c.nickname === opponentName)
  )
  if (!amIBusy || !opponent) { return }

  // Disregard if gameState variable is an invalid type or array length
  gameState = JSON.parse(gameState)
  if ((typeof gameState !== 'object') || (gameState.length !== 9)) {
    console.log('Invalid gameState')
    return
  }

  // Disregard if player attempts move during opponent's turn
  if ((player1Turn && (nickname === players.player2)) ||
    (!player1Turn && (nickname === players.player1))) {
    console.log('Not your turn')
    return
  }

  // Update client, opponent objects
  const results = calculateGameOver(gameState, players)
  client.gameState = opponent.gameState = gameState
  client.winner = opponent.winner = results.winner
  client.winningLine = opponent.winningLine = results.line
  client.tie = opponent.tie = results.tie
  // Switch turn if game is still progressing
  if (!results.winner) {
    client.player1Turn = opponent.player1Turn = !player1Turn
  }

  // Send
  const messageTitle = `${client.nickname} completed their turn`
  const playerClients = [client, opponent]
  buildAndSendMessage(clients, client, playerClients, messageTitle)
}

// Approve a rematch request
function approveRematch (clients, client) {
  const opponentName = client.opponent
  let players = client.players

  // Disregard if client doesn't have an opponent
  if (!opponentName || !players.player1 || !players.player2) { return }

  // Disregard if client isn't busy or cannot find opponent client
  const amIBusy = client.readyStatus === constants.STATUS_BUSY
  const opponent = clients.find(
    c => (c.nickname === opponentName)
  )
  if (!amIBusy || !opponent) { return }

  // Disregard if a rematch wasn't requested
  if (!client.rematchRequested || !client.rematchRequester) { return }

  // Update client, opponent objects
  players = { player1: client.nickname, player2: opponent.nickname }
  client.gameState = opponent.gameState = Array(9).fill(null)
  client.player1Turn = opponent.player1Turn = true
  client.players = opponent.players = players
  client.winner = opponent.winner = null
  client.rematchRequested = opponent.rematchRequested = false
  client.rematchRequester = opponent.rematchRequester = null
  client.tie = opponent.tie = false
  client.winningLine = opponent.winningLine = null

  // Send
  const messageTitle = `${client.nickname} approved rematch with ${opponent.nickname}`
  buildAndSendMessage(clients, client, clients, messageTitle)
}

// Check game state to see if a game over has been reached, and if so
// returns an object containing the winner line and the symbol of the
// winner player
// * Private scope *
function calculateGameOver (gameState, players) {
  const symbolToPlayer = {
    X: players.player1,
    O: players.player2
  }
  // Indicies of winning lines
  const lines = [
    [0, 1, 2],
    [3, 4, 5],
    [6, 7, 8],
    [0, 3, 6],
    [1, 4, 7],
    [2, 5, 8],
    [0, 4, 8],
    [2, 4, 6]
  ]

  // Check for winning lines
  for (let i = 0; i < lines.length; i++) {
    const [a, b, c] = lines[i]
    if (
      (gameState[a] &&
      (gameState[a] === gameState[b]) &&
      (gameState[a] === gameState[c]))
    ) {
      return {
        line: lines[i].join(''),
        tie: false,
        winner: symbolToPlayer[gameState[a]]
      }
    }
  }

  // Tie occurs if all gameState values are truthy
  return {
    line: null,
    tie: gameState.every(val => val),
    winner: null
  }
}

// Engage in a match against another user
function engageOpponent (clients, client, messageArray) {
  // Disregard if opponent name argument is absent
  if (messageArray.length < 2) { return }

  // Disregard if client is busy or cannot find opponent client
  const amIBusy = client.readyStatus === constants.STATUS_BUSY
  const opponentName = messageArray[1]
  const opponent = clients.find(
    c => (c.nickname === opponentName) && (c.readyStatus === constants.STATUS_READY)
  )
  if (amIBusy || !opponent) { return }

  // Update client, opponent objects
  const players = {
    player1: client.nickname,
    player2: opponent.nickname
  }
  const gameState = Array(9).fill(null)
  client.exited = opponent.exited = false
  client.gameState = opponent.gameState = gameState
  client.lonePlayer = opponent.lonePlayer = false
  client.opponent = opponent.nickname
  opponent.opponent = client.nickname
  client.player1Turn = opponent.player1Turn = true
  client.players = opponent.players = players
  client.readyStatus = opponent.readyStatus = constants.STATUS_BUSY
  client.tie = opponent.tie = false
  client.winner = opponent.winner = null
  client.winningLine = opponent.winningLine = null

  // Send
  const messageTitle = `Match begun between: ${client.nickname}, ${opponent.nickname}`
  buildAndSendMessage(clients, client, clients, messageTitle)
}

// Exit completed game and return to lobby
function exitGame (clients, client) {
  const opponentName = client.opponent
  const players = client.players

  // Disregard if client doesn't have an opponent
  if (!opponentName || !players.player1 || !players.player2) { return }

  // Disregard if client isn't busy or cannot find opponent client
  const amIBusy = client.readyStatus === constants.STATUS_BUSY
  const opponent = clients.find(
    c => (c.nickname === opponentName)
  )
  if (!amIBusy || !opponent) { return }

  // Update client, opponent objects
  client.readyStatus = constants.STATUS_READY
  client.lonePlayer = true
  opponent.lonePlayer = true

  // Send
  const messageTitle = `${client.nickname} exited the game`
  const playerClients = [client, opponent]
  buildAndSendMessage(clients, client, playerClients, messageTitle)
}

// Forfeit match to opponent user
function forfeit (clients, client) {
  const opponentName = client.opponent
  const players = client.players

  // Disregard if client doesn't have an opponent
  if (!opponentName || !players.player1 || !players.player2) { return }

  // Disregard if client isn't busy or cannot find opponent client
  const amIBusy = client.readyStatus === constants.STATUS_BUSY
  const opponent = clients.find(
    c => (c.nickname === opponentName)
  )
  if (!amIBusy || !opponent) { return }

  // Update client, opponent objects
  client.winner = opponent.winner = opponent.nickname

  // Send
  const messageTitle = `${client.nickname} forfeit to ${opponent.nickname}`
  const playerClients = [client, opponent]
  buildAndSendMessage(clients, client, playerClients, messageTitle)
}

// Send opponent a request to start a rematch
function requestRematch (clients, client) {
  const opponentName = client.opponent
  const players = client.players

  // Disregard if client doesn't have an opponent
  if (!opponentName || !players.player1 || !players.player2) { return }

  // Disregard if client isn't busy or cannot find opponent client
  const amIBusy = client.readyStatus === constants.STATUS_BUSY
  const opponent = clients.find(
    c => (c.nickname === opponentName)
  )
  if (!amIBusy || !opponent) { return }

  // Update client, opponent objects
  client.rematchRequested = opponent.rematchRequested = true
  client.rematchRequester = opponent.rematchRequester = client.nickname

  // Send
  const messageTitle = `${client.nickname} requests a rematch with ${opponent.nickname}`
  const playerClients = [client, opponent]
  buildAndSendMessage(clients, client, playerClients, messageTitle)
}

export default {
  applyTurn,
  approveRematch,
  engageOpponent,
  exitGame,
  forfeit,
  requestRematch
}
