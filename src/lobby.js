import * as constants from './constants'
import { buildAndSendMessage, nicknamedClients } from './utils'

function setNickname (clients, client, messageArray) {
  // Disregard if new nickname argument is absent
  if (messageArray.length < 2) { return }

  const newNickname = messageArray[1]
  const oldNickname = client.nickname
  const nicknameTaken = nicknamedClients(clients).includes(newNickname)

  // Disregard if nickname is unchanged or alreay taken
  if ((newNickname === oldNickname) || nicknameTaken) { return }

  // Update client object
  client.nickname = newNickname

  // Send
  const messageTitle = `Changed nickname to ${newNickname}`
  buildAndSendMessage(clients, client, clients, messageTitle)
}

function setStatus (clients, client, messageArray) {
  // Disregard if status argument (ie. idle/ready/busy) is absent
  if (messageArray.length < 2) { return }

  const readyStatus = messageArray[1]
  // Disregard bad combo's
  if ((
    (readyStatus === constants.STATUS_READY) &&
    (client.readyStatus === constants.STATUS_READY)) ||
    ((readyStatus === constants.STATUS_IDLE) &&
    (client.readyStatus === constants.STATUS_IDLE))) { return }

  // Update client object
  client.readyStatus = readyStatus

  // Send
  const messageTitle = `${client.nickname} is now ${readyStatus}`
  buildAndSendMessage(clients, client, clients, messageTitle)
}

export default {
  setNickname,
  setStatus
}
