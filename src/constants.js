// Message send types
export const SEND_MSG = 'message'
export const SEND_NICK = 'nick'
export const SEND_NOTIFY = 'notify'

// Message receive types
export const RECEIVE_APPROVE_REMATCH = '/approveRematch'
export const RECEIVE_ENGAGE = '/engage'
export const RECEIVE_EXIT = '/exitGame'
export const RECEIVE_FORFEIT = '/forfeit'
export const RECEIVE_NICK = '/nick'
export const RECEIVE_REQ_REMATCH = '/requestRematch'
export const RECEIVE_STATUS = '/status'
export const RECEIVE_TURN = '/turn'

// Socket event type
export const EVENT_CLOSE = 'close'
export const EVENT_CONN = 'connection'
export const EVENT_MSG = 'message'

// Ready status values
export const STATUS_IDLE = 'idle'
export const STATUS_BUSY = 'busy'
export const STATUS_READY = 'ready'
