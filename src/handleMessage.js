import * as constants from './constants'

import Lobby from './lobby'
import Game from './game'

// Determine command in message and execute
function handleMessage (clients, client, message) {
  // Disregard if message is empty
  if (message.length === 0) { return }

  // Parse message
  const messageArray = message.trim().split(' ')
  const command = messageArray[0]

  switch (command) {
    case constants.RECEIVE_APPROVE_REMATCH:
      Game.approveRematch(clients, client)
      break
    case constants.RECEIVE_ENGAGE:
      Game.engageOpponent(clients, client, messageArray)
      break
    case constants.RECEIVE_EXIT:
      Game.exitGame(clients, client)
      break
    case constants.RECEIVE_FORFEIT:
      Game.forfeit(clients, client)
      break
    case constants.RECEIVE_NICK:
      Lobby.setNickname(clients, client, messageArray)
      break
    case constants.RECEIVE_REQ_REMATCH:
      Game.requestRematch(clients, client)
      break
    case constants.RECEIVE_STATUS:
      Lobby.setStatus(clients, client, messageArray)
      break
    case constants.RECEIVE_TURN:
      Game.applyTurn(clients, client, messageArray)
      break
    default:
      // Disregard other messages
      break
  }
}

export { handleMessage }
